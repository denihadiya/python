import sys
from ascii_magic import AsciiArt

def generate_ascii_art(image_path):
    try:
        # Mencoba membaca gambar dan mengonversinya menjadi ASCII art
        my_art = AsciiArt.from_image(image_path)
        my_art.to_terminal()

    except FileNotFoundError:
        # Jika file gambar tidak ditemukan
        print(f"File gambar '{image_path}' tidak ditemukan. Pastikan file ada di direktori yang benar.")

    except Exception as e:
        # Jika terjadi kesalahan lainnya
        print(f"Terjadi kesalahan: {e}")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        # Jika tidak ada argumen jalur gambar yang diberikan melalui perintah terminal
        print("Silakan berikan jalur gambar sebagai argumen melalui perintah terminal.")
        print("Contoh: python latihan.py images/bapak.jpg")
    else:
        # Ambil jalur gambar dari argumen yang diberikan melalui perintah terminal
        image_path = sys.argv[1]
        generate_ascii_art(image_path)
